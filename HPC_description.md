# Make your R code run faster: Description

## 1. Overview

The goal of this workshop is to teach intermediate level programmers how to make their R code run faster. 

During this workhop we go through three ways of making your code run faster:

- We explore how to profile (where are the bottlenecks) and optimise your code.
- We also explore how to make the best of your computerâs power with parallelisation.
- The final part of the course explores what other options there are at the University of Melbourne for running your code in the cloud or at a high performance computer. This includes a brief introduction to *spartan* and to the *Melbourne Research Cloud*.  

### Learning Objectives

This training aims to enable students to:

- Understand how run time calculations work in R.

- Identify bottlenecks that make their code run slowly.

- Apply various methodologies to remove bottlenecks and make their code run faster.

- Improve the speed of their code by using parallelisation.

- Implement a task using a super computer.

### Learning Outcomes

- The ability to generate reproducible analysis of quantitative data using statistical and visualisation tools in an efficient way.
  
- Computational skills: The ability to adapt existing computational methods and tools to complete a target task.
  
- Problem solving skills: the ability to engage with unfamiliar problems and identify relevant solution strategies.
  
- Collaborative skills: the ability to work in a team.

### Training Format

The workshop consists of a 3-hour session followed. Workshops are delivered 2-4 times a year (Feb-Nov) and participants are encouraged to return to practice their skills.

- **Duration:** 3 hours.
  
- **Frequency:** Approxiamtely every two months.
  
- **Mode of presentation:** Physical presence with a 90% focus on hands-on problem solving.
  
- **Materials**: Participants can follow the [course material online](https://gitlab.unimelb.edu.au/rescom-training/r/run-your-r-code-faster---profiling-and-hpc)


## 2. Eligibility requirements

This workshop is accessible to everyone with some basic R experience.

### Prerequisites

Please note that you must have attended the R introductory workshop or have some basic R knowledge in order to attend this workshop.


### Recommended background knowledge

We recommend participants to have an introductory-level knowledge of R programming and Rstudio.

We recommend consulting the course materials for the [introduction to R workshop](https://nikkirubinstein.gitbooks.io/resguides-introductory-r-workshop/content/content/01-rstudio-intro.html).


### Participation requirements
- **Participants must bring a laptop and charger** with a Mac, Linux, or Windows operating system (not a tablet, Chromebook, ipad etc.) in which they have administrative privileges on.
- **Participants should [install R and Rstudio](https://resbaz.github.io/installRinstructions/) before the workshop.**

## 3. Assessment and Expectations

- We encourage participants to come with an open mind and participate in discussions with others.
  
- This workshop is a hands on training. We expect participants to code and ask questions to the trainer and, more importantly, to other students.

- Particpants are expected to work in teams during the day.

- Participants will be asked to self-assess their progress and understanding of concepts.

- Students are expected to provide feedback to improve the course.

## 4. Dates and Times

For dates, information and registration in upcoming training, please refer to our [Eventbrite page](http://rescomunimelb.eventbrite.com/)